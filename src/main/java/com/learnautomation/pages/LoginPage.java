package com.learnautomation.pages;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.learnautomation.testcases.BaseClass;
import com.learnautomation.utility.HelperUtility;

public class LoginPage extends BaseClass
{
	
	
	public LoginPage(WebDriver ldriver)
	{
		this.driver = ldriver;
		
	}
	
	@FindBy(name="username")
	public static WebElement uname;
	
	@FindBy(name="password")
	public static WebElement passwd;
	
	@FindBy(xpath="//input[@value='Login' and @type='submit']")
	public static WebElement btnLogin;
	
	
	public void loginCrm(String username, String password) throws IOException, InterruptedException
	{
		Thread.sleep(2000);
		uname.sendKeys(username);
		passwd.sendKeys(password);
		HelperUtility.waitAndClick(btnLogin, driver);
	
	}
	
}
