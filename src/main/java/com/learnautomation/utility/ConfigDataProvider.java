package com.learnautomation.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ConfigDataProvider 
{
	Properties pro;
	
	public ConfigDataProvider()
	{
		File src = new File(System.getProperty("user.dir")+ "/src/main/resources/Config/config.properties");
		try {
			FileInputStream fis = new FileInputStream(src);
			
			pro = new Properties();
			pro.load(fis);
			
			} 
		
		catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Not able to load file >>"+ e.getMessage());
			
		} 	
	}
	
	public String getBrowser()
	{
		return pro.getProperty("Browser");	
	}
	
	public String getUrl()
	{
		return pro.getProperty("appUrl");
	}
	
	public String getDataFromConfig(String keyToSearch)
	{
		return pro.getProperty(keyToSearch);
	}
	
}
