package com.learnautomation.utility;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonDataProvider
{
	JSONParser parser = new JSONParser();
	FileReader reader;
	Object obj;
	JSONObject jsonobj;
	
	public JsonDataProvider() throws IOException, ParseException
	{
		reader = new FileReader(System.getProperty("user.dir")+ "/src/main/resources/" + "userprofiles/" + "QAUser.json");
		obj = parser.parse(reader);
		jsonobj = (JSONObject) obj;
	
	}

	public String getUsername() 
	{
		String username = (String)jsonobj.get("username");
		return username;
	}
	
	public String getPasswd() 
	{
		String passwd = (String)jsonobj.get("password");
		return passwd;
	}
		

}	