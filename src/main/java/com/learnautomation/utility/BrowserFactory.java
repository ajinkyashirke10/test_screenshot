package com.learnautomation.utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.learnautomation.testcases.autoConstVar;

public class BrowserFactory 
{

	public static WebDriver startApplication(WebDriver driver, String browserName, String url)
	{
		if(browserName.equals("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", autoConstVar.sChromeDriverPath);
			driver = new ChromeDriver();
		}
		
		else if(browserName.equals("Firefox"))
		{
			System.setProperty("webdriver.gecko.driver", autoConstVar.sGeckoDriverPath);
			driver = new FirefoxDriver();
		}
		
		else if(browserName.equals("IE"))
		{
			System.setProperty("webdriver.ie.driver", autoConstVar.sIEDriverPath);
			driver = new FirefoxDriver();
		}
		
		else
		{
			System.out.println("We do not support these browsers");
			
		}
		
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		return driver;
		
	}
	
	public static void closeBrowser(WebDriver driver)
	{
		
		driver.close();
	}
	
}
