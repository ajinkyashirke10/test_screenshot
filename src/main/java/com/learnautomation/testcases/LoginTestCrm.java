package com.learnautomation.testcases;

import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.learnautomation.pages.LoginPage;
import com.learnautomation.utility.BrowserFactory;
import com.learnautomation.utility.HelperUtility;
import com.learnautomation.utility.JsonDataProvider;


public class LoginTestCrm extends BaseClass
{
	
	@Test(priority=1)
	public void loginApp() throws IOException, InterruptedException, ParseException
	{
		LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.loginCrm(j1.getUsername(),j1.getPasswd());
			
	}
	
	
	@Test(priority=2)
	public void gotoCRMPage() throws IOException, InterruptedException
	{
		
		// start reporters
	    ExtentHtmlReporter report = new ExtentHtmlReporter("./target/learnautomation1.html");

	    // create ExtentReports and attach reporter(s)
	    ExtentReports extent = new ExtentReports();
	    extent.attachReporter(report);
	    
	    // creates a toggle for the given test, adds all log events under it    
        ExtentTest logger = extent.createTest("Goto CRM Page");
        logger.log(Status.INFO, "get title");
        
        Thread.sleep(3000);
		str = driver.getTitle();
		
		logger.info(str);
		
		if(str.equalsIgnoreCase("CRMPRO"))
		{
			logger.log(Status.PASS, "Title verified" );
			HelperUtility.captureScreenshot(driver, "SS1");
			logger.pass("Test Passed", MediaEntityBuilder.createScreenCaptureFromPath("./Screenshots/SS1.png").build());
			extent.flush();
		}
		
		else {
			
			logger.log(Status.FAIL, "Title not verified" );
			logger.fail("Test Failed", MediaEntityBuilder.createScreenCaptureFromPath("./Screenshots/SS1.png").build());
			extent.flush();
		}
	} 
	
}
