package com.learnautomation.testcases;

import org.testng.annotations.Test;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.learnautomation.pages.LoginPage;
import com.learnautomation.utility.BrowserFactory;
import com.learnautomation.utility.ConfigDataProvider;
import com.learnautomation.utility.HelperUtility;
import com.learnautomation.utility.JsonDataProvider;


public class BaseClass
{

	public WebDriver driver;
	public String str;
	public JsonDataProvider j1;
	public ConfigDataProvider config;
	
	@BeforeSuite
	public void setUpSuite() throws IOException, ParseException
	{
		j1 = new JsonDataProvider();
		config = new ConfigDataProvider();
		
	}
	
	
	@BeforeClass
	public void launchBrowser()
	{
		Reporter.log("Start test");
		driver = BrowserFactory.startApplication(driver, config.getBrowser(), config.getUrl());
	}
	
	
	@AfterClass
	public void closeBrowser()
	{
		BrowserFactory.closeBrowser(driver);
		
	}
	
	@AfterMethod
	public void tearDownMethod(ITestResult result) throws IOException
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			HelperUtility.captureScreenshot(driver, "SS1");
		}
		
		else if(result.getStatus()==ITestResult.SUCCESS)
		{
			HelperUtility.captureScreenshot(driver, "SS1");
		}
	}
	
}
