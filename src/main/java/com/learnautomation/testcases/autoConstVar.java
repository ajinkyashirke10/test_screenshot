package com.learnautomation.testcases;

public class autoConstVar 
{
	public static String sChromeDriverPath = "./Exes/chromedriver.exe";
	public static String sIEDriverPath     = "./Exes/IEDriverServer.exe";
	public static String sGeckoDriverPath  = "./Exes/geckodriver.exe";
	
	public static int iProxyPort 				= 8080;
	public static long lngPageLoadTimeout 		= 60L;
	public static long lngImplicitWaitTimeout	= 30L;
	
	public static String testDataPath = ".\\Testdata\\";
	public static String testDataFileName = "inputData.xlsx";
	
}
